from django.shortcuts import render, redirect
from .forms import JadwalForm
from .models import Jadwal
from datetime import date, datetime

# Create your views here.
def list_jadwal(request):
    jadwal = Jadwal.objects.all()

    response = {
        'jadwal':jadwal,
    }
    return render(request, 'kegiatan/list_jadwal.html', response)

def buat_jadwal(request):
    form = JadwalForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = JadwalForm()

    context = {
        'form':form,
    }

    return render(request, 'kegiatan/buat_jadwal.html', context)
    