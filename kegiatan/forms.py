from django import forms
import datetime
from .models import Jadwal

# class JadwalForm(forms.Form):
#     nama_kegiatan = forms.CharField(
#         label="Nama Kegiatan", 
#         max_length=30, 
#         widget=forms.TextInput(
#             attrs={
#                 'class':'form-control', 
#                 'placeholder':'Masukkan nama kegiatan anda'
#                 }
#         )
#     )

#     tanggal = forms.DateField(
#         label='Tanggal', 
#         widget=forms.SelectDateWidget(
#             attrs={'class':'form-control col-sm-2'},
#             years=range(2018, 2026, 1)), 
#         initial=datetime.datetime.now
#     )

#     jam = forms.CharField(
#         label='Pukul',
#         widget=forms.TextInput(
#             attrs={
#                 'class':'form-control', 
#                 }
#         )
#     )

#     deskripsi_kegiatan = forms.CharField(
#         label='Deskripsi Kegiatan',
#         widget=forms.Textarea(attrs={
#             'class':'form-control'
#         })
#     )

#     agree = forms.BooleanField(
#         widget=forms.CheckboxInput(
#             attrs={
#                 'class':'custom-control-input',
#                 'id':'customControlAutosizing'
#             }
#         )
#     )

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields=[
            'name',
            'start_time',
            'end_time',
            'description'
        ]

"""
Tipe-tipe data field :
IntegerField()
DecimalField()
FLoatField()
BooleanField()
CharField()

Bisa ditambah argument 
1.(required=False), default True
misal : CharField(required=False)

2.(max_length=10)

String input :
email_field = forms.EmailField()
regex_field = forms.RegexField()
slug_field = forms.SlugField()
url_field = forms.URLField()
ip_field = forms.GenericIPAddressField()

Select input :
choice_field = forms.ChoiceField(choices=PILIHAN), bikin list/tupel PILIHAN
misal : PILIHAN = (('nilai1', 'Pilihan1'), ('nilai2', 'Pilihan2'), ...)
multi_choice_field = forms.MultipleChoiceField(choices=PILIHAN)
multi_typed_choice = forms.TypedMultipleChoiceField(choices=PILIHAN)
nulll_boolean_field = forms.NullBooleanField()

date time :
date_field = forms.DateField()
datetime_field = forms.DateTimeField()
duration_field = forms.DurationField()
time_field = dorms.TimeField()
splitdatetime_field = forms.SplitDateTimeField()

File input :
file_field = forms.FileField()
image_field = forms.ImageField()
"""