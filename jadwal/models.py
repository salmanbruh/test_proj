from django.db import models

# Create your models here.
class Jadwal(models.Model):
    LIST_KATEGORI = (
        ('Hiburan', 'Hiburan'),
        ('Tugas', 'Tugas'),
        ('Kelas', 'Kelas'),
        ('Lain-lain', 'Lain-lain')
    )
    kategori = models.CharField(max_length=30, choices=LIST_KATEGORI)
    name = models.CharField(max_length=30)
    place = models.CharField(max_length=30)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
