from django.test import TestCase, Client
from jadwal.models import Jadwal
from jadwal.forms import JadwalForm
# Create your tests here.

class JadwalModelTest(TestCase):
    def setup_test_data(self):
        Jadwal.objects.create(kategori='b', nama='a', place='coeg', start_time='10/6/2001', end_time='10/7/2001')

    def test_if_nama_exist(self):
        jadwal = Jadwal.objects.get(id=1)
        name = jadwal._meta.get_field('name').verbose_name
        self.assertEqual(name, 'name')

    def tesst_if_nama_max_length_is_30(self):
        jadwal = Jadwal.objects.get(id=1)
        name = jadwal._meta.get_field('title').max_length
        self.assertEqual(name, 30)

    response=[]
    def test_if_models_on_database(self):
        jadwal = Jadwal.objects.create(kategori='b',
                                       nama='a',
                                       place='coeg',
                                       start_time='10/6/2001',
                                       end_time='10/7/2001')

        counting_post = Jadwal.objects.count()
        self.assertEqual(counting_post, 1)

    def test_if_url_exits(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
